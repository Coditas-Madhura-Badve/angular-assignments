Directives in Angular
Directives are classes that add additional behavior to elements in your Angular applications.

Four types:
Component directives
Structural directives
Attribute directives
custom attribute

component directives:
Most used directive used to create component templates

structural directives:
They are used to modify or update dom structure
they are defined with * prefix
this prefix wraps the code inside a template
for e.g <div *ngIf="hero" class="name">{{hero.name}}</div>
* converts this code into 
<ng-template [ngIf]="hero">
  <div class="name">{{hero.name}}</div>
</ng-template>
This convention is shorthand that Angular interprets and converts into a longer form.

examples-
NgIf
NgFor
NgSwitch




On surface hidden property can seem similar to structural directives used to hidden certain data
but they are very different

when we use hidden property on element it stays in DOM just not seen on page i.e hidden
but when we hide data using structural directives it gets removed from DOM
for small elements tree we can use hidden property
but for large elements tree use stuctural directives


