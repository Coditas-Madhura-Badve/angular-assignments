import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
  title = 'directives';

  courses = [1,2];
  viewMode = '';

  changeViewMode(mode:string){
    this.viewMode = mode;
  }


  flowers = [
    {id:1, itsName: 'Rose'},
    {id:2, itsName: 'Mogara'},
    {id:3, itsName: 'Marigold'}
  ];

  addFlower(){
    this.flowers.push({id:4, itsName: 'Lily'});
  }

  delFlower(flowerName:string){
    let pos = this.flowers.findIndex((ele) => ele.itsName === flowerName)
    this.flowers.splice(pos,1);
  }
}
